/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.exercice;

import java.util.logging.Level;
import java.util.logging.Logger;


/* exercice 0 */

public class Main {
    
    final static Logger ex = Logger.getLogger ("Main");

    public static void main(final String[] args) {
        
//        ex.log(Level.INFO, args[0] ); 

//        factorization(args); 

//      factorizationuser(); /exercice 2/
      
      /*exercice 3 */
      
      Address adressetype1 = new Address(1, 69, "rue", "demaman",666, "Paris", "France", 1.6868f , 46.8114f); /* f (pour forcer le faite d'étre en float ) */
      
      Address adressetype2 = new Address(2, 22, "ozrio", "kanaki", 12256, "Tokyo", "Japon"); /* on crée une adresse */
      
       ex.log(Level.INFO, adressetype1.toString() ); /* permet afficher  le toString de L'adresse Type 1 */
       
       ex.log(Level.INFO, adressetype2.toString() ); /* permet  afficher le toString de L'adresse Type 2 */
       
       /*Fin 3 */
       
       /*exercice 4 */
       
       Place place1 = new Place(1, "Ecole", adressetype1, Place.Type.Ecole, "commentaire"); /* on crée une Place */
       ex.log(Level.INFO, place1.toString() ); /* permet afficher le toString de L'adresse Type 1 */
       
       /*Fin 4 */
       
       
       /*exercice 5 */
       
       Person person1 = new Person(1, "Christophe", "Dacruz", 180, adressetype2); /* on crée une Persone */
         ex.log(Level.INFO, person1.toString() ); /* permet afficher le toString dela person1 */

       /*fin 5 */
       
       
       /*exercice 6 */
       
       
       /*teacher*/
       
       Teacher teacher1 = new Teacher(1, "Christophe", "Dacruz", 35, adressetype2, "PLS");
        ex.log(Level.INFO, teacher1.toString() ); /* permet afficher le toString dela teacher1 */
        
        
        /*Student*/
        
        Student student1 = new Student(1, "quentin", "pradet",666, adressetype1, "Licorne");
          ex.log(Level.INFO, student1.toString() ); /* permet afficher le toString dela student1 */
        
       
      /*Fin 6 */ 
       
       
       
    }
    
    
    
    /* Exercice 1 */
   
	public static void factorization (String[] args){  /*prend un argument  actuellement 3 */
            
		int a = Integer.parseInt(args[0]);
		int fact = 1;
		for(int i = a; i >= 1; i--){
			fact = fact*i;
		}
		ex.log(Level.INFO, " je suis " + fact + ""); /*ressort le résultat attendu 6  */
	} /* Fin exercice 1 */
        
        
        
        /* Exercice 2 */
        
        
        	public static void factorizationuser (){ 
            	
                    java.util.Scanner entree = new java.util.Scanner(System.in);
                    int a = 1;
                    while (a != 0) {
                    System.out.println("Donne moi un nombre stp =) ");
                     a = entree.nextInt();
                    entree.nextLine();
 
		int res = 1;
		for(int i = a; i >= 1; i--){
			res = res*i;
		}
		ex.log(Level.INFO, " je suis " + res + " bisous"); 
                    }
                
	} /* Fin exercice 2 */
            
               

}









