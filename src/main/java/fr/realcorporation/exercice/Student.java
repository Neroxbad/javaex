/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.exercice;

/**
 *
 * @author qpradet
 */


/*exerccice 6 */


public class Student extends Person{
    
       private String classe ;
    
    public Student(int personid, String nom, String prenom, int age, Address adresse , String classe ) {
        super(personid, nom, prenom, age, adresse);
        this.classe = classe;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

   
    
}
