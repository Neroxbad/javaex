/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.exercice;

/**
 *
 * @author qpradet
 */


/*exerccice 6 */


public class Teacher extends Person {
    
    private String matiere ;
    
    public Teacher(int personid, String nom, String prenom, int age, Address adresse , String matiere ) {
        super(personid, nom, prenom, age, adresse);
        this.matiere = matiere;
    }

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }
    
    
    
}
